document.addEventListener('DOMContentLoaded', function() {
  @@include('modules/svg.js');
  @@include('modules/polyfills.js');
  @@include('modules/aos.js');
});